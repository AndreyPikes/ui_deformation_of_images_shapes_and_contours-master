﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

[RequireComponent(typeof(CanvasRenderer))]
//[ExecuteInEditMode]
public class UIImageMinimize : MaskableGraphic
{
    /// <summary>
    /// Загруженная текстура
    /// </summary>
    public Texture texture;

    [SerializeField, Range(0, 1)]
    private float Ywarping;

    [SerializeField, Range(0, 1)]
    private float Xwarping;

    /// <summary>
    /// Загруженная текстура
    /// </summary>
    public override Texture mainTexture
    {
        get { return texture; }
    }

    public int xSize, ySize; 
    

    public AnimationCurve CurveY = new AnimationCurve();
    public float ForceCurve = 1.0f;

    /// <summary>
    /// массив векторов2 ВСЕ ТОЧКИ
    /// </summary>
    protected Vector2[] vertices;    //ВОТ ЭТО МЕНЯЕТСЯ ПРИ АНИМАЦИИ

    /// <summary>
    /// массив векторов2 ВСЕ ТОЧКИ
    /// </summary>
    protected Vector3[] verticesVisualize;
    protected bool isInitVertices = false;


    protected override void OnPopulateMesh(VertexHelper vertexHelper) //заполнить сетку ФИГАЧИТ В КАЖДОМ КАДРЕ и обновляет размер сетки
    {
        float minX = (0f - rectTransform.pivot.x) * rectTransform.rect.width; //координаты углов экрана
        float minY = (0f - rectTransform.pivot.y) * rectTransform.rect.height;
        float maxX = (0f + rectTransform.pivot.x) * rectTransform.rect.width;
        float maxY = (0f + rectTransform.pivot.y) * rectTransform.rect.height;
        
        var color32 = (Color32)color;

        if (xSize < 1) xSize = 1;
        if (ySize < 1) ySize = 1; //можно ограничить просто в инспекторе

        vertexHelper.Clear(); //почистили старый

        

        if (isInitVertices && vertices.Length != (xSize + 1) * (ySize + 1))
        {
            isInitVertices = false;
        }

        if (!isInitVertices)
        {

            verticesVisualize = new Vector3[(xSize + 1) * (ySize + 1)];
            vertices = new Vector2[(xSize + 1) * (ySize + 1)];
        }

        Vector2[] uv = new Vector2[vertices.Length];
        //Vector4[] tangents = new Vector4[vertices.Length];
        //Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
        for (int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++) //X - строки Y - столбцы I - все вместе
            {


                // CurveX.Evaluate(AddedTime + (1.0f / (float)xSize) * (float)x) - 1.0f; //единица возвращает в центр
                float xWarp = - y * Xwarping; //сжатие сверху вниз


                float yWarp = ((-2f / (float)xSize) * x + 1) * (CurveY.Evaluate((1.0f/(float)ySize)*(float)y)  -  1.0f) * (1 - Xwarping); //Сужение по горизонтали, но проходимся по Y 
                yWarp += ((-2f / (float)xSize) * x + 1) * Xwarping / ForceCurve * 10;
                yWarp = yWarp * ForceCurve * Ywarping;

                                 //+   Xwarping * 5 * y / ySize / ForceCurve)


                //if (!isInitVertices) //не очень понятное условие вроде не сказывается
                {
                    vertices[i] = new Vector2((float)x, (float)y);
                    //vertices[i].x = (float)2*points[i].position.x / (float)Screen.width;
                    //vertices[i].y = (float)2 *points[i].position.y / (float)Screen.height;

                }

                

                uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
                //tangents[i] = tangent; //все тангенты задаем new Vector4(1f, 0f, 0f, -1f);

                //вертисы для визуализации на сцене
                verticesVisualize[i] = new Vector2((minX + ((vertices[i].x + yWarp) * ((maxX - minX) / xSize))), (minY + ((vertices[i].y + xWarp) * ((maxY - minY) / ySize))));
                //а вот эти уже для деформации
                vertexHelper.AddVert(new Vector3((minX + ((vertices[i].x + yWarp) * ((maxX - minX) / xSize))), (minY + ((vertices[i].y + xWarp) * ((maxY - minY) / ySize)))), color32, new Vector2(uv[i].x, uv[i].y));
            }
        }

        int[] triangles = new int[xSize * ySize * 6];
        for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
                triangles[ti + 5] = vi + xSize + 2;
                vertexHelper.AddTriangle(triangles[ti], triangles[ti + 1], triangles[ti + 2]);
                vertexHelper.AddTriangle(triangles[ti + 3], triangles[ti + 4], triangles[ti + 5]);
            }
        }
        isInitVertices = true;


    }

    //protected override void OnPopulateMesh(VertexHelper vertexHelper) //заполнить сетку ФИГАЧИТ В КАЖДОМ КАДРЕ и обновляет размер сетки
    //{
    //    float minX = (0f - rectTransform.pivot.x) * rectTransform.rect.width; //координаты углов экрана
    //    float minY = (0f - rectTransform.pivot.y) * rectTransform.rect.height;
    //    float maxX = (1f - rectTransform.pivot.x) * rectTransform.rect.width;
    //    float maxY = (1f - rectTransform.pivot.y) * rectTransform.rect.height;

    //    var color32 = (Color32)color;

    //    if (xSize < 1) xSize = 1;
    //    if (ySize < 1) ySize = 1; //можно ограничить просто в инспекторе

    //    vertexHelper.Clear(); //почистили старый



    //    if (isInitVertices && vertices.Length != (xSize + 1) * (ySize + 1))
    //    {
    //        isInitVertices = false;
    //    }

    //    if (!isInitVertices)
    //    {

    //        verticesRun = new Vector3[(xSize + 1) * (ySize + 1)];
    //        vertices = new Vector2[(xSize + 1) * (ySize + 1)];
    //    }

    //    //Vector2[] 
    //    uv = new Vector2[vertices.Length];
    //    //Vector4[] tangents = new Vector4[vertices.Length];
    //    //Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);
    //    for (int i = 0, y = 0; y <= ySize; y++)
    //    {
    //        for (int x = 0; x <= xSize; x++, i++) //X - строки Y - столбцы I - все вместе
    //        {


    //            float xTime = CurveX.Evaluate(AddedTime + (1.0f / (float)xSize) * (float)x) - 1.0f; //единица возвращает в центр
    //            float yTime = CurveY.Evaluate(AddedTime + (1.0f / (float)ySize) * (float)y) - 1.0f;

    //            float yTimeNoise = 0.0f;
    //            float xTimeNoise = 0.0f;
    //            if (isEnablePerlinNoise)
    //            {
    //                yTimeNoise = Mathf.PerlinNoise(Time.time * (float)x, 0) * ForcePerlinNoise;
    //                xTimeNoise = Mathf.PerlinNoise(0, Time.time * (float)y) * ForcePerlinNoise;
    //            }

    //            xTime = xTime * ForceCurve;
    //            yTime = yTime * ForceCurve;

    //            if (!isInitVertices) //не очень понятное условие вроде не сказывается
    //            {

    //                vertices[i] = new Vector2((float)x, (float)y);
    //                verticesRun[i] = new Vector2((float)x, (float)y);
    //            }
    //            uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
    //            //tangents[i] = tangent; //все тангенты задаем new Vector4(1f, 0f, 0f, -1f);

    //            //вертисы для визуализации на сцене
    //            verticesRun[i] = new Vector2((minX + ((vertices[i].x + yTime + yTimeNoise) * ((maxX - minX) / xSize))), (minY + ((vertices[i].y + xTime + xTimeNoise) * ((maxY - minY) / ySize))));
    //            vertexHelper.AddVert(new Vector3((minX + ((vertices[i].x + yTime + yTimeNoise) * ((maxX - minX) / xSize))), (minY + ((vertices[i].y + xTime + xTimeNoise) * ((maxY - minY) / ySize)))), color32, new Vector2(uv[i].x, uv[i].y));
    //        }
    //    }

    //    int[] triangles = new int[xSize * ySize * 6];
    //    for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++)
    //    {
    //        for (int x = 0; x < xSize; x++, ti += 6, vi++)
    //        {
    //            triangles[ti] = vi;
    //            triangles[ti + 3] = triangles[ti + 2] = vi + 1;
    //            triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
    //            triangles[ti + 5] = vi + xSize + 2;
    //            vertexHelper.AddTriangle(triangles[ti], triangles[ti + 1], triangles[ti + 2]);
    //            vertexHelper.AddTriangle(triangles[ti + 3], triangles[ti + 4], triangles[ti + 5]);
    //        }
    //    }
    //    isInitVertices = true;


    //}

    /// <summary>
    /// Отрисовка сетки с точками в инспекторе
    /// </summary>
    private void OnDrawGizmosSelected()
    {
        float minX = (0f - rectTransform.pivot.x) * rectTransform.rect.width;
        float minY = (0f - rectTransform.pivot.y) * rectTransform.rect.height;
        float maxX = (1f - rectTransform.pivot.x) * rectTransform.rect.width;
        float maxY = (1f - rectTransform.pivot.y) * rectTransform.rect.height;
        float disX = ((maxX - minX) / xSize) * 0.1f;
        float disY = ((maxY - minY) / ySize) * 0.1f;
        float dis = disX > disY ? disY : disX;


        int[] triangles = new int[xSize * ySize * 6];
        for (int ti = 0, vi = 0, y = 0; y < ySize; y++, vi++)
        {
            for (int x = 0; x < xSize; x++, ti += 6, vi++)
            {
                triangles[ti] = vi;
                triangles[ti + 3] = triangles[ti + 2] = vi + 1;
                triangles[ti + 4] = triangles[ti + 1] = vi + xSize + 1;
                triangles[ti + 5] = vi + xSize + 2;
                Gizmos.color = new Color(0.0f, 0.0f, 0.0f, 0.6f);
                Vector3 p1 = verticesVisualize[triangles[ti]] + transform.position;
                Vector3 p2 = verticesVisualize[triangles[ti + 1]] + transform.position;
                Vector3 p3 = verticesVisualize[triangles[ti + 2]] + transform.position;
                Gizmos.DrawLine(p1, p2);
                Gizmos.DrawLine(p1, p3);
                Gizmos.DrawLine(p3, p2);
                Gizmos.DrawSphere(p1, dis);
                Gizmos.DrawSphere(p2, dis);
                Gizmos.DrawSphere(p3, dis);
                p1 = verticesVisualize[triangles[ti + 3]] + transform.position;
                p2 = verticesVisualize[triangles[ti + 4]] + transform.position;
                p3 = verticesVisualize[triangles[ti + 5]] + transform.position;
                Gizmos.DrawLine(p1, p2);
                Gizmos.DrawLine(p1, p3);
                Gizmos.DrawLine(p3, p2);
                Gizmos.DrawSphere(p1, dis);
                Gizmos.DrawSphere(p2, dis);
                Gizmos.DrawSphere(p3, dis);
            }
        }
    }

    private void Update()
    {
        SetAllDirty();

    }
}
